# DIY Air Quality Monitor
My KiCAD take on the Arduino based air quality monitor from How to Mechatronics
https://howtomechatronics.com/projects/diy-air-quality-monitor-pm2-5-co2-voc-ozone-temp-hum-arduino-meter/

See my 'Various' KiCAD library for the required symbols and footprints.
https://gitlab.com/kicad16/various